package com.autosconcesionario.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-19T00:38:09")
@StaticMetamodel(Vehiculos.class)
public class Vehiculos_ { 

    public static volatile SingularAttribute<Vehiculos, Double> precio;
    public static volatile SingularAttribute<Vehiculos, String> color;
    public static volatile SingularAttribute<Vehiculos, String> imagen;
    public static volatile SingularAttribute<Vehiculos, String> vin;
    public static volatile SingularAttribute<Vehiculos, String> fabricante;
    public static volatile SingularAttribute<Vehiculos, String> modelo;

}