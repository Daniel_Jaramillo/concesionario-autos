/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autosconcesionario.ejb;

import com.autosconcesionario.entity.Vehiculos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Daniel Jaramillo
 */
@Stateless
public class VehiculosFacade extends AbstractFacade<Vehiculos> implements VehiculosFacadeLocal {

  @PersistenceContext(unitName = "AutosConcesionarioPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public VehiculosFacade() {
    super(Vehiculos.class);
  }
  
   @Override
   public List<String> vehiculosPorModelos(String marca) {
       List<String>listaModelo;
        Query q=em.createQuery("SELECT DISTINCT(a.modelo) FROM Vehiculos a where a.fabricante=:m");
        q.setParameter("m", marca);
        listaModelo=q.getResultList();
        return listaModelo;

    }
   
   @Override
   public List<String> obtenerColorPorModeloFabricante(String modelo, String fabricante) {
        List<String> colorModeloFabricante;  
        Query q = em.createQuery("SELECT a.color FROM Vehiculos a WHERE a.modelo=:m and a.fabricante=:f GROUP BY a.color");
        q.setParameter("m", modelo);
        q.setParameter("f", fabricante);
        colorModeloFabricante = (List<String>) q.getResultList();

        return colorModeloFabricante;
    }
   
   @Override
   public List<Vehiculos> vehiculosFabricanteModeloColor(String fabricante, String modelo, String color) {
         List<Vehiculos>listaFiltrado;
         Query q=em.createQuery("SELECT a  FROM Vehiculos a where a.fabricante=:f and a.modelo=:m and a.color=:c");
         q.setParameter("f",fabricante);
         q.setParameter("m", modelo);
         q.setParameter("c", color);
         listaFiltrado=q.getResultList();
         return listaFiltrado;
     }
  
}
