/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autosconcesionario.ejb;

import com.autosconcesionario.entity.Vehiculos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Daniel Jaramillo
 */
@Local
public interface VehiculosFacadeLocal {

  void create(Vehiculos vehiculos);

  void edit(Vehiculos vehiculos);

  void remove(Vehiculos vehiculos);

  Vehiculos find(Object id);

  List<Vehiculos> findAll();

  List<Vehiculos> findRange(int[] range);
  
  List<String> vehiculosPorModelos(String marca);
  
  List<String> obtenerColorPorModeloFabricante(String modelo, String fabricante);
  
  List<Vehiculos> vehiculosFabricanteModeloColor(String fabricante, String modelo, String color);

  int count();
  
}
