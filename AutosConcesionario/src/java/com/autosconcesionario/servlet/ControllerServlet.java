package com.autosconcesionario.servlet;

import com.autosconcesionario.ejb.ClientesFacadeLocal;
import com.autosconcesionario.ejb.VehiculosFacadeLocal;
import com.autosconcesionario.ejb.VentasgeneralesFacadeLocal;
import com.autosconcesionario.entity.Clientes;
import com.autosconcesionario.entity.Vehiculos;
import com.autosconcesionario.entity.Ventasgenerales;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Daniel Jaramillo, Cesar Benavidez
 */
@MultipartConfig
public class ControllerServlet extends HttpServlet {

  @EJB
  private VentasgeneralesFacadeLocal ventasgeneralesFacade;

  @EJB
  private VehiculosFacadeLocal vehiculoFacade;

  @EJB
  private ClientesFacadeLocal clientesFacade;

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    String url = "clientes.jsp";

//        String data = "Hello World! Jue jue jue";
//        response.getWriter().write(data);
    try (PrintWriter out = response.getWriter()) {

      String action = request.getParameter("action");
      url = "listaVentas.jsp";

      if ("addCliente".equals(action)) {
        url = "clientes.jsp";
        Clientes cliente = new Clientes();
        cliente.setDocumento(request.getParameter("documento"));
        cliente.setNombres(request.getParameter("nombres"));
        cliente.setApellidos(request.getParameter("apellidos"));
        cliente.setCorreo(request.getParameter("correo"));
        cliente.setTelefono(request.getParameter("telefono"));
        clientesFacade.create(cliente);

        //redirección
        RequestDispatcher rs = request.getRequestDispatcher("clientes.jsp");
        rs.forward(request, response);

      } else if ("modificaCliente".equals(action)) {

        Clientes cliente = new Clientes();
        cliente.setDocumento(request.getParameter("documento"));
        cliente.setNombres(request.getParameter("nombres"));
        cliente.setApellidos(request.getParameter("apellidos"));
        cliente.setCorreo(request.getParameter("correo"));
        cliente.setTelefono(request.getParameter("telefono"));

//        Clientes cliente = clientesFacade.find(documento);
        clientesFacade.edit(cliente);

        //redirección
        RequestDispatcher rs = request.getRequestDispatcher("clientes.jsp");
        rs.forward(request, response);

      } else if ("addVehiculo".equals(action)) {
        url = "vehiculos.jsp";
        Vehiculos vehiculo = new Vehiculos();
        vehiculo.setVin(request.getParameter("vin"));
        String fabricante = request.getParameter("fabricante");
        vehiculo.setFabricante(fabricante);
        String modelo = request.getParameter("modelo");
        vehiculo.setModelo(modelo);
        String color = request.getParameter("color");
        vehiculo.setColor(color);
        vehiculo.setPrecio(Double.valueOf(request.getParameter("precio")));
        Part filePart = request.getPart("adjunto");
        String extension = obtenerExtension(Paths.get(filePart.getSubmittedFileName()).getFileName().toString());
        String fileName = fabricante + modelo + color + "." + extension;
        String instanceRoot = System.getProperty("com.sun.aas.instanceRoot");
        File uploads = new File(instanceRoot + "/docroot/static");
        File file = new File(uploads, fileName);

        try (InputStream input = filePart.getInputStream()) {
          Files.copy(input, file.toPath());
        } catch (Exception e) {
          System.err.println("ERROR: " + e.getMessage());
        }

        vehiculo.setImagen(fileName);
        vehiculoFacade.create(vehiculo);

        //redirección
        RequestDispatcher rs = request.getRequestDispatcher("vehiculos.jsp");
        rs.forward(request, response);

      } else if ("ventas".equals(action)) {
        url = "Ventas.jsp";
        Ventasgenerales ventas = new Ventasgenerales();

        String fecha = request.getParameter("fechaVenta");
        DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaDate = null;
        try {
          fechaDate = formato.parse(fecha);
        } catch (ParseException ex) {
          System.out.println(ex);
        }

        ventas.setFechaVenta(fechaDate);

        Clientes cliente = new Clientes();
        cliente.setDocumento(request.getParameter("cliente"));
        Vehiculos vehiculo = new Vehiculos();
        vehiculo.setVin(request.getParameter("vehiculo"));
        ventas.setClienteDocumento(cliente);
        ventas.setVehiculoCodigo(vehiculo);
        ventasgeneralesFacade.create(ventas);

      } else if ("listaVentas".equals(action)) {

        //List<Ventasgenerales>findAll=ventasgeneralesFacade.findAll();
        //request.getSession().setAttribute("ventas",findAll);                
        //url="listaVentas.jsp";
        List<Ventasgenerales> findAll = ventasgeneralesFacade.findAll();
        request.getSession().setAttribute("ventas", findAll);
        url = "listaVentas.jsp";

      } else if ("matricula".equals(action)) {
//                int matricula = Integer.parseInt(request.getParameter("matricula"));
//                Vehiculos matriculaVehiculo;
//                matriculaVehiculo = vehiculoFacade.consultaVehiculo(matricula);
//                request.getSession().setAttribute("vehiculo", matriculaVehiculo);
        url = "vehiculoMatricula.jsp";

      } else if ("fabricante".equals(action)) {
        //ArrayList<String> fabricante = new ArrayList<String>();  
        int matricula = Integer.parseInt(request.getParameter("matricula"));

//                String prueba = vehiculoFacade.primerFabricante();
//                List<String> listaFabricante = vehiculoFacade.obtenerFabricantes();
//                request.getSession().setAttribute("listaFabricante", listaFabricante);
//                url = "consultaVehiculos.jsp";
      } else if ("contenidoAJAX".equals(action)) {
        System.out.println("Testing working.");
      }
    } finally {
      response.sendRedirect(url);
      out.close();
    }
  }

  private String obtenerExtension(String nombreArchivo) {
    String extension = "";

    int i = nombreArchivo.lastIndexOf('.');
    if (i > 0) {
      extension = nombreArchivo.substring(i + 1);
    }
    return extension;
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    String respuesta = "";

    String action = request.getParameter("action");

    if (null != action) {
      switch (action) {
        case "consultarCliente":
          String id = request.getParameter("id");
          Clientes cliente = clientesFacade.find(id);
          respuesta = new Gson().toJson(cliente);
          break;
        case "consultarImagen":
          String img = request.getParameter("img");
          respuesta = "https://static.iris.net.co/soho/upload/images/2017/2/22/46998_1.jpg";
          response.setContentType("text/html");
          break;
        case "deleteUser":
          id = request.getParameter("id");
          cliente = clientesFacade.find(id);
          clientesFacade.remove(cliente);
          //redirección
          RequestDispatcher rs = request.getRequestDispatcher("clientes.jsp");
          rs.forward(request, response);
          break;
        case "consultarFabricantes":
          List<Vehiculos> listaCarrosTotal = vehiculoFacade.findAll();///Estan todo los carros
          List<Ventasgenerales> listaCarrosVendidos = ventasgeneralesFacade.findAll();//Vendidos

          Set<String> fabricantes = new HashSet<>();

          for (int i = 0; i < listaCarrosTotal.size(); i++) {
            int contador = 0;
            for (int j = 0; j < listaCarrosVendidos.size(); j++) {
              String ct = listaCarrosTotal.get(i).getVin();
              String cv = listaCarrosVendidos.get(j).getVehiculoCodigo().getVin();
              if (ct.equals(cv)) {
                contador++;
              }
            }
            if (contador == 0) {
              fabricantes.add(listaCarrosTotal.get(i).getFabricante());
            }
          }

          respuesta = new Gson().toJson(fabricantes);
          response.setContentType("application/json");
          break;

        case "consultarModelosPorFabricante":
          String fabricante = request.getParameter("fabricante");
          List<String> tmp = vehiculoFacade.vehiculosPorModelos(fabricante);
          respuesta = new Gson().toJson(tmp);
          response.setContentType("application/json");
          break;

        case "consultarColoresPorModeloFabricante":
          String modelo = request.getParameter("modelo");
          fabricante = request.getParameter("fabricante");
          List<String> coloresDisponibles = vehiculoFacade.obtenerColorPorModeloFabricante(modelo, fabricante);
          respuesta = new Gson().toJson(coloresDisponibles);
          response.setContentType("application/json");
          break;

        case "consultarVehiculo":
          fabricante = request.getParameter("fabricante");
          modelo = request.getParameter("modelo");
          String color = request.getParameter("color");

          List<Vehiculos> listaVehiculos = vehiculoFacade.vehiculosFabricanteModeloColor(fabricante, modelo, color);
          Vehiculos vehiculo = listaVehiculos.get(0);
          respuesta = new Gson().toJson(vehiculo);
          response.setContentType("application/json");
          break;
        case "addVenta":
          Ventasgenerales venta = new Ventasgenerales(1);

          Date date = new Date();

          venta.setFechaVenta(date);

          String vehiculo_str = request.getParameter("vehiculo");
          vehiculo = vehiculoFacade.find(vehiculo_str);
          venta.setVehiculoCodigo(vehiculo);

          String documento_str = request.getParameter("documento");
          cliente = clientesFacade.find(documento_str);
          venta.setClienteDocumento(cliente);
          
          ventasgeneralesFacade.create(venta);

          break;
        default:
          respuesta = "default answer";
          break;
      }
    }

    response.setCharacterEncoding("UTF-8");
    response.getWriter().write(respuesta);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "This servlet was created by students of La Universidad de Antioquia. Please entoy it.";
  }// </editor-fold>

}
