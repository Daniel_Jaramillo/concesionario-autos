<%-- 
    Document   : clientes
    Created on : 31/08/2018, 12:52:01 AM
    Author     : Cesar Benavidez
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registro clientes</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">

    <style>
      .btn_modificar{
        background-color: #6b3d91 !important;
        border-color: #6b3d91;
      }

      .oculto {
        display: none;
      }
    </style>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">Bienvenido</a>
      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5" style="font-size: 5em">Clientes</h1>
          </div> 
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
            <form action='ControllerServlet?action=addCliente' method="post" id='clientesForm' name='clientesForm'>
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="text" class="form-control form-control-lg" placeholder="Ingrese número de cédula"
                         name="documento">
                </div>
                <div class="col-12 col-md-3">
                  <h3 class="mb-5">Documento</h3>
                </div>
              </div>                
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="text" class="form-control form-control-lg" placeholder="Ingrese nombre(s)"
                         name="nombres">
                </div>
                <div class="col-12 col-md-3">
                  <h3 class="mb-5">Nombre(s)</h3>
                </div>
              </div>
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="text" class="form-control form-control-lg" placeholder="Ingrese apellido(s)"
                         name="apellidos">
                </div>
                <div class="col-12 col-md-3">
                  <h3 class="mb-5">Apellido(s)</h3>
                </div>
              </div>         
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="text" class="form-control form-control-lg" placeholder="Ingrese número de celular"
                         name="telefono">
                </div>
                <div class="col-12 col-md-3">
                  <h3 class="mb-5">Teléfono</h3>
                </div>
              </div>              
              <div class="form-row">
                <div class="col-12 col-md-9 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Ingrese correo electrónico"
                         name="correo">
                </div>
                <div class="col-12 col-md-3">
                  <h3 class="mb-5">Correo</h3>
                </div>
              </div>
              <div class="col-3 col-md-3">
                <div style="display: flex;">
                  <button id="btn_ppl" class="btn btn-lg btn-primary">Registrar</button>
                  <button id="btn_rem" class="btn btn-lg btn-danger oculto" style="margin-left: 8px">Eliminar</button>
                </div>
              </div>
            </form>              
          </div>
        </div>
      </div>
    </header>

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <a href="clientes.jsp">
                <img src="img/cliente.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Registro de clientes</h3>
              <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">              
              <a href="vehiculos.jsp">
                <img src="img/vehiculo.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Registro de vehículos</h3>
              <p class="lead mb-0">Featuring the latest build of the new Bootstrap 4 framework!</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">              
              <a href="consultar.jsp">
                <img src="img/busqueda2.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Consulta vehículo</h3>
              <p class="lead mb-0">Ready to use with your own content, or customize the source files!</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2018. All Rights Reserved.</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </body>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script>

    $("#clientesForm").submit(function () {
      return false;
    });

    $("#btn_ppl").click(function () {
      if ($(this).hasClass("btn_modificar")) { // MODIFICAR cliente
        let documento = $('input[name="documento"]').val();
        let nombres = $('input[name="nombres"]').val();
        let apellidos = $('input[name="apellidos"]').val();
        let telefono = $('input[name="telefono"]').val();
        let correo = $('input[name="correo"]').val();
        $.post("ControllerServlet?action=modificaCliente",
        {
          "documento": documento,
          "nombres": nombres,
          "apellidos": apellidos,
          "telefono": telefono,
          "correo": correo
        });
        alert("¡Usuario modificado correctamente!");
      } else { //AGREGAR cliente
        let validacion = validarDatos();
        if ("1" !== validacion) {
          alert(validacion);
          return 0;
        }
        alert("¡Usuario registrado correctamente!");
        document.clientesForm.submit();
      }
    });

    $("#btn_rem").click(function () {
      let documento = $('input[name="documento"]').val();
      alert("¡Usuario eliminado correctamente!");
      document.location.href = "ControllerServlet?action=deleteUser&id=" + documento;
    });

    //retorna "1" en caso de todo ser válido, de lo contrario, retorna
    //un mensaje de error
    function validarDatos() {
      let validacion;

      let documento = $('input[name="documento"]').val();
      validacion = validarCedula(documento);
      if ("1" !== validacion) {
        return 'El documento ' + validacion;
      }

      let nombre = $('input[name="nombres"]').val();
      validacion = validarCaracteres(nombre);
      if ("1" !== validacion) {
        return 'El nombre ' + validacion;
      }

      let apellidos = $('input[name="apellidos"]').val();
      validacion = validarCaracteres(apellidos);
      if ("1" !== validacion) {
        return 'Los apellidos ' + validacion;
      }

      let numero = $('input[name="telefono"]').val();
      validacion = validarCelular(numero);
      if ("1" !== validacion) {
        return 'El número de teléfono ' + validacion;
      }

      let correo = $('input[name="correo"]').val();
      validacion = validarCorreo(correo);
      if ("1" !== validacion) {
        return 'El correo ' + validacion;
      }

      return "1";
    }

    //retorna "1" en caso de todo ser válido, de lo contrario, retorna
    //un mensaje de error
    function validarCedula(numero) {
      //chequea si el parámetro son solo números
      return (/^\d+$/.test(numero)) ? "1" : "debe únicamente contener dígitos";
    }

    //retorna "1" en caso de todo ser válido, de lo contrario, retorna
    //un mensaje de error
    function validarCaracteres(cadena) {
      //chequea si el parámetro son solo letras o espacios
      return (/^[a-z ,.'-]+$/i.test(cadena)) ? "1" : "debe únicamente contener letras o espacios";
    }

    //retorna "1" en caso de todo ser válido, de lo contrario, retorna
    //un mensaje de error
    function validarCelular(numero) {
      //chequea si el parámetro son solo números
      if (/^\d+$/.test(numero)) {
        //el número de celular debe tener al menos 6 dígitos
        return numero.length > 5 ? "1" : "debe contener al menos 6 dígitos";
      }
      return "debe únicamente contener dígitos";
    }

    //retorna "1" en caso de todo ser válido, de lo contrario, retorna
    //un mensaje de error
    function validarCorreo(correo) {
      //chequea si el parámetro es un correo válido
      return (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
              .test(correo)) ? "1" : "debe ser un correo válido";
    }


  </script>

  <!--MODIFICACIÓN DE USUARIO-->
  <script>
    $('input[name="documento"]').focusout(function () {
      let id = $('input[name="documento"]').val();
      $.get("ControllerServlet?action=consultarCliente&id=" + id, function (cliente) {
        if (cliente !== "null") {
          activarModificacionCliente();
          rellenarCamposCliente(cliente);
        } else {
          activarRegistroCliente();
          console.log('No hay clientes registrados con esta cédula');
        }
      });
    });

    function activarModificacionCliente() {
      let boton = $("#btn_ppl");
      let rem = $("#btn_rem");
      boton.html("Modificar");
      boton.addClass("btn_modificar");
      rem.removeClass("oculto");
    }

    function rellenarCamposCliente(_cliente) {
      let cliente = JSON.parse(_cliente);
      $('input[name="nombres"]').val(cliente.nombres);
      $('input[name="apellidos"]').val(cliente.apellidos);
      $('input[name="telefono"]').val(cliente.telefono);
      $('input[name="correo"]').val(cliente.correo);
    }

    function activarRegistroCliente() {
      let boton = $("#btn_ppl");
      let rem = $("#btn_rem");
      boton.html("Registrar");
      boton.removeClass("btn_modificar");
      rem.addClass("oculto");

      $('input[name="nombres"]').val("");
      $('input[name="apellidos"]').val("");
      $('input[name="telefono"]').val("");
      $('input[name="correo"]').val("");
    }
  </script>

</html>
