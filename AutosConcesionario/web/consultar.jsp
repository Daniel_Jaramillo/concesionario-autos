<%-- 
    Document   : consultar
    Created on : 31/08/2018, 08:24:02 AM
    Author     : Daniel Jaramillo
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.lang.String"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Consulta de vehículos</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="css/landing-page.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
      <div class="container">
        <a class="navbar-brand" href="#">Bienvenido</a>
      </div>
    </nav>

    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 id="titulo" class="mb-5" style="font-size: 5em">¡Consulta!</h1>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">

            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0">

                <select id="comboFabricante" class="form-control form-control-lg">
                  <option>Escoge un fabricante</option>
                </select>

              </div>
              <div class="col-12 col-md-3">
                <h3 class="mb-5">Fabricante</h3>

              </div>
            </div>                
            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0">

                <select id="comboModelo" class="form-control form-control-lg">
                  <option>Escoge un modelo</option>
                </select>

              </div>
              <div class="col-12 col-md-3">
                <h3 class="mb-5">Modelo</h3>
              </div>
            </div>
            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0">

                <select id="comboColor" class="form-control form-control-lg">
                  <option>Escoge un color</option>
                </select>

              </div>
              <div class="col-12 col-md-3">
                <h3 class="mb-5">Color</h3>
              </div>
            </div>         
            <div class="form-row">
              <!--              <div id="btn_ajax" class="col-2 col-md-9 mb-2 mb-md-0">
                              <button type="text" class="btn btn-light">AJAX</button>
                            </div>-->
            </div>              

            <div>&nbsp</div>
            <div class="col-12 col-md-3">
              <button id="consultar" type="submit" class="btn btn-block btn-lg btn-primary">Consultar</button>
              <!--<button id="restablecer">Restablecer</button>-->
            </div>
          </div>

        </div>
      </div>
    </header>

    <!-- The Modal -->
    <div class="modal fade" id="ventanaModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h1 id="modeloModal" class="modal-title">...</h1>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col">
                <img id="imagenModal"
                     src="https://admin.kienyke.com/wp-content/uploads/2018/02/Carro-tesla.jpg"
                     style="width: 100%; height: 100%;">
              </div>
              <div class="col infoAuto">
                <div>
                  <h4>FABRICANTE</h2>
                    <p id="fabricanteModal">...</p>
                </div>
                <div>
                  <h4>PRECIO</h2>
                    <p id="precioModal">....</p>
                </div>
                <div>
                  <h4>VIN</h2>
                    <p id="vinModal">...</p>
                </div>
              </div>
            </div>
          </div>

          <style>
            .infoAuto h4 {
              color: #999999;
            }
          </style>

          <!-- Modal footer -->
          <div class="modal-footer">
            <input class="form-control input-sm nonActive" id="inputDocumento" type="text" placeholder="Documento"
                   type="number">
            <button id="vender" type="button" class="btn btn-primary">Vender</button>
          </div>

          <style>
            .nonActive {
              display: none;
            }
          </style>

        </div>
      </div>
    </div>

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <a href="clientes.jsp">
                <img src="img/cliente.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Registro de clientes</h3>
              <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">              
              <a href="vehiculos.jsp">
                <img src="img/vehiculo.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Registro de vehículos</h3>
              <p class="lead mb-0">Featuring the latest build of the new Bootstrap 4 framework!</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">              
              <a href="consultar.jsp">
                <img src="img/busqueda2.png" alt="HTML tutorial" style="width:120px;height:120px;border:0">
              </a>
              <h3>Consulta vehículo</h3>
              <p class="lead mb-0">Ready to use with your own content, or customize the source files!</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="footer bg-light">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
            <ul class="list-inline mb-2">
              <li class="list-inline-item">
                <a href="#">About</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Contact</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
              <li class="list-inline-item">&sdot;</li>
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
            </ul>
            <p class="text-muted small mb-4 mb-lg-0">&copy; Your Website 2018. All Rights Reserved.</p>
          </div>
          <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-facebook fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item mr-3">
                <a href="#">
                  <i class="fa fa-twitter fa-2x fa-fw"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram fa-2x fa-fw"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>    

    <script>

      function obtenerDominio() {
        let url = window.location.href;
        var pos = url.lastIndexOf("/");
        var domain = url.substr(0, pos + 1);
        return domain;
      }
      ;

      // Click en el botón de vender
      $("#vender").click(function () {
        let element = $("#inputDocumento");
        let boton = $("#vender");
        
        if (boton.html() === "Vender") {
          element.toggleClass("nonActive");
          element.focus();
          boton.html("Confirmar");
        } else {
          let documento = $("#inputDocumento").val();
          let vehiculo = $("#vinModal").html();
          $.get("ControllerServlet?action=addVenta&documento="+documento+"&vehiculo="+vehiculo, function() {
            alert("Se ha ingresado correctamente la venta");
            location.reload();
          });
        }



      });

      // Cuando la ventana modal se cierra
      $("#ventanaModal").on("hidden.bs.modal", function () {
        //se reinician los valores de la ventana modal
        $("#vender").html("Vender");
        let element = $("#inputDocumento");
        element.addClass("nonActive");
        element.val("");
      });

      // Click en el botón consultar
      $("#consultar").click(function () {
        $("#ventanaModal").modal('show');

        let fabricante = $("#comboFabricante").find(":selected").text();
        let modelo = $("#comboModelo").find(":selected").text();
        let color = $("#comboColor").find(":selected").text();

        $.get("ControllerServlet?action=consultarVehiculo&fabricante=" + fabricante + "&modelo=" + modelo + "&color=" + color, function (responseJson) {

          $("#modeloModal").text(responseJson.modelo);
          $("#fabricanteModal").text(responseJson.fabricante);
          $("#precioModal").text(responseJson.precio);
          $("#vinModal").text(responseJson.vin);

          let imagenModal = obtenerDominio() + "/static/" + responseJson.imagen;
          console.log(imagenModal);
          $("#imagenModal").attr("src", imagenModal);
        });

      });

      // Este scipt se ejecuta cuando la página termina de cargar el DOM
      // se hace un llamado AJAX haciendo petición de los fabricantes disponibles
      $(document).ready(function () {
        $.get("ControllerServlet?action=consultarFabricantes", function (responseJson) {
          var comboBox = $("#comboFabricante");
          var option;

          for (var i = 0; i < responseJson.length; i++) {
            option = document.createElement("option");
            option.setAttribute("value", i);
            option.innerHTML = responseJson[i];
            comboBox.append(option);
          }
        });
      });

      // Combobox fabricante es cambiado
      $("#comboFabricante").change(function () {

        // restrablecer demás comboboxes (dejar solamente primera opción)
        restablecer($("#comboModelo"));
        restablecer($("#comboColor"));

        // agregar nuevos modelos
        let fabricante = $("#comboFabricante").find(":selected").text();
        $.get("ControllerServlet?action=consultarModelosPorFabricante&fabricante=" + fabricante, function (responseJson) {
          var comboBox = $("#comboModelo");
          var option;

          for (var i = 0; i < responseJson.length; i++) {
            option = document.createElement("option");
            option.setAttribute("value", i);
            option.innerHTML = responseJson[i];
            comboBox.append(option);
          }
        });
      });

      // Combobox modelo es cambiado
      $("#comboModelo").change(function () {

        restablecer($("#comboColor"));

        let fabricante = $("#comboFabricante").find(":selected").text();
        let modelo = $("#comboModelo").find(":selected").text();

        $.get("ControllerServlet?action=consultarColoresPorModeloFabricante&modelo=" + modelo + "&fabricante=" + fabricante, function (responseJson) {
          var comboBox = $("#comboColor");
          var option;

          for (var i = 0; i < responseJson.length; i++) {
            option = document.createElement("option");
            option.setAttribute("value", i);
            option.innerHTML = responseJson[i];
            comboBox.append(option);
          }
        });
      });


      // Combobox color es cambiado
      $("#comboColor").change(function () {
//        console.log("Combobox color clickeado");
      });

      $("#restablecer").click(function () {
        $.get("ControllerServlet?action=consultarColoresPorModeloFabricante", function (responseJson) {
          console.log(responseJson);
        });
      });

      // Se restrablece el combobox, es decir, se deja solamente la primera opción
      function restablecer(combobox) {
        let id = $(combobox).attr("id");
        combobox = document.getElementById(id); //el nodo es obtenido como un objeto de Vanilla Javascript
        let options = combobox.options;
        let numeroOpciones = options.length;
        for (var i = numeroOpciones - 1; i > 0; i--) {
          combobox.removeChild(options[i]);
        }
      }
    </script>

  </body>

</html>
